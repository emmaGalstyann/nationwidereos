$transition: all 0.3s ease-in-out;
.animation{
    .absolute-elements{
      opacity: 0;
      transition: all 0.3s ease-in-out;
    }
    &.intro{
      h1, .btn{
        transform: translateY(50px);
        opacity: 0;
        transition: $transition;
      }
      ul li{
        transform: translateY(20px);
        opacity: 0;
        transition: $transition;
      }
      .absolute-block{
        opacity: 0;
        transform: translateY(50px);
        transition: $transition;
        .left, .right{
          &>img{
            &:nth-child(2), &:nth-child(3){
              opacity: 0;
              transition: all 0.7s ease-out;
            }
          }
        }
      }
      &+.slider{
        transform: translateY(100px);
        opacity: 0;
        transition: $transition;
        transition-delay: 1.5s;
      }  
      &.animated{
        h1, .btn{
          transform: translateY(0);
          opacity: 1;
        }
        ul li{
          transform: translateY(0);
          opacity: 1;
          @for $i from 1 to 5 {
            &:nth-of-type(#{$i}) {
              transition-delay: $i * 0.3s;
            }
          }
        }
        .btn{
          transition-delay: 1.2s;
        }
        .absolute-block{
          opacity: 1;
          transition-delay: 0.6s;
          transform: translateY(0);
        }
        .right, .left{
          //transform: scale(1);
          //opacity: 1;
          &>img{
            &:nth-child(2){
              opacity: 1;
              transition-delay: 0.9s;
            }
            &:nth-child(3){
              opacity: 1;
              transition-delay: 1.2s;
            }
          }
        }
        /*.center{
          transform: scale(1);
          opacity: 1;
          transition-delay: 0.8s;
        }*/
        /*.left{
          //transform: scale(1);
          //opacity: 1;
          //transition-delay: 0.2s;
          &>img{
            &:nth-child(2){
              opacity: 1;
              transition-delay: 0.8s;
            }
            &:nth-child(3){
              opacity: 1;
              transition-delay: 1s;
            }
          }
        }*/
        &+.slider{
            transform: translateY(0);
            opacity: 1;
        }
      }
    }
    .center-position{
      h2, h3, h5{
        transform: translateY(50px);
        opacity: 0;
        transition: $transition;
      }
    }
    .blocks{
      transform: translateY(50px);
      opacity: 0;
      transition: $transition;
    }
    .btn{
      transform: translateY(50px);
      opacity: 0;
      transition: $transition;
    }
    &.selling {
      .container>svg{
        opacity: 0;
        transition: $transition;
      }
    }
    &.blocks-template.blocks-template-arrown{
      .blocks{
        transform: translateY(0);
        opacity: 1;
        &>div{
          opacity: 0.3;
          transform: translateX(-20px);
          transition: $transition;
          .hover-blocks{
            div{
              div{
                &:nth-child(1),&:nth-child(2),&:nth-child(3){
                  opacity: 0;
                  transition: $transition;
                }
              }
            }
          }
          .arrown:after{
            right: 100%;
            transition: $transition;
          }
        }
      }
    }
    .col-7{
      h3, h6{
        transform: translateY(50px);
        opacity: 0;
        transition: all 0.5s ease-out;
      }
    }
    .col-5{
      .vertical-blocks{
        div{
          transform: translateY(50px);
          opacity: 0;
          transition: all 0.5s ease-out;
        }
      }
    }
    &.bg-blue{
      h4, ul li, .btn{
        transform: translateY(50px);
        opacity: 0;
        transition: $transition;
      }
      ul:before, ul:after {
        opacity: 0;
        transition: $transition;
      }
      &.animated{
        h4, ul li, .btn{
          transform: translateY(0);
          opacity: 1;
        }
        .center-position h2{
          transition-delay: 0.3s;
        }
        ul:before{
          opacity: 1;
          transition-delay: 0.4s;
        }
        ul li{
          @for $i from 1 to 5 {
            &:nth-of-type(#{$i}) {
              transition-delay: $i * 0.3s + 0.3;
            }
          }
        }
        ul:after {
          opacity: 1;
          transition-delay: 1.4s;
        }
        .btn{
          transition-delay: 1.5s;
        }
      }
    }
    &#contact{
      form > p{
        opacity: 0;
        transform: translateY(20px);
        transition: $transition;
        @for $i from 1 to 10 {
          &:nth-of-type(#{$i}) {
            transition-delay: $i * 0.3s + 0.3;
          }
        }
      }
      form > div.flex p{
        opacity: 0;
        transform: translateY(20px);
        transition: $transition;
        transition-delay: 1.5s;
      }
      &.animated{
        form > p, form > div.flex p{
          opacity: 1;
          transform: translateY(0);
        }
      }
    }
    &.animated{
      .absolute-elements{
        opacity: 1;
        transition-delay: 0.6s;
      }
      .center-position{
        h2, h3, h5{
          transform: translateY(0);
          opacity: 1;
        }
        h2{
          transition-delay: 0.6s;
          &.underline div:after{
            transition-delay: 1s;
          }
        }
        h2 + h3{
          transition-delay: 0.9s;
        }
        h5{
          transition-delay: 0.3s;
        }
      }
      .btn{
        transform: translateY(0);
        opacity: 1;
      }
      .blocks{
        transform: translateY(0);
        opacity: 1;
        transition-delay: 0.9s;
      }
      &.selling {
        .container>svg{
          opacity: 1;
        }
        .btn{
          transition-delay: 1.2s;
        }
      }
      &.blocks-template.blocks-template-arrown{
        .blocks{
          &>div{
            &:nth-child(1){
              opacity: 1;
              transform: translateX(0);
              transition-delay: 0.6s;
              .hover-blocks{
                div{
                  div{
                    &:nth-child(1),&:nth-child(2),&:nth-child(3){
                      opacity: 1;
                    }
                    &:nth-child(1){
                      transition-delay: 0.6s;
                    }
                    &:nth-child(2){
                      transition-delay: 0.9s;
                    }
                    &:nth-child(3){
                      transition-delay: 1.2s;
                    }
                  }
                }
              }
              .arrown{
                filter: grayscale(0);
                opacity: 1;
                transition-delay: 1.3s;
              }
              .arrown:after{
                right: 0;
                transition-delay: 0.9s;
              }
            }
            &:nth-child(2){
              opacity: 1;
              transform: translateX(0);
              transition-delay: 1.5s;
              .hover-blocks{
                div{
                  div{
                    &:nth-child(1),&:nth-child(2),&:nth-child(3){
                      opacity: 1;
                    }
                    &:nth-child(2){
                      transition-delay: 1.5s;
                    }
                    &:nth-child(2){
                      transition-delay: 1.8s;
                    }
                    &:nth-child(3){
                      transition-delay: 2.1s;
                    }
                  }
                }
              }
              .arrown{
                filter: grayscale(0);
                opacity: 1;
                transition-delay: 2.2s;
              }
              .arrown:after{
                right: 0;
                transition-delay: 1.8s;
              }
            }
            &:nth-child(3){
              opacity: 1;
              transform: translateX(0);
              transition-delay: 2.4s;
              .hover-blocks{
                div{
                  div{
                    &:nth-child(1),&:nth-child(2),&:nth-child(3){
                      opacity: 1;
                    }
                    &:nth-child(2){
                      transition-delay: 2.4s;
                    }
                    &:nth-child(2){
                      transition-delay: 2.7s;
                    }
                    &:nth-child(3){
                      transition-delay: 3s;
                    }
                  }
                }
              }
            }
          }
        }
      }
      .col-7{
        h3, h6{
          transform: translateY(0);
          opacity: 1;
        }
        h6{
          @for $i from 1 to 5 {
            &:nth-of-type(#{$i}) {
              transition-delay: $i * 0.3s;
            }
          }
        }
      }
      .col-5{
        .vertical-blocks{
          div{
            @for $i from 1 to 5 {
              &:nth-of-type(#{$i}) {
                transform: translateY(0);
                opacity: 1;
                transition-delay: $i * 0.3s + 0.3;
              }
            }
          }
        }
      }
      .underline div:after{
        width: 100%;
        transition-delay: 0.4s;
      }
      &#features{
        svg path.dots{
          opacity: 1;
          transition-delay: 0.8s;
        }
      }
      &#contact .hover-blocks div>div{
        opacity: 1;
        &:nth-child(2){
          transition-delay: 1.2s;
        }
        &:nth-child(3){
          transition-delay: 1.5s;
        }
      }
    }
  }
  
  header.animation{
    .container{
      transform: translateY(35px);
      opacity: 0;
      transition: all 0.5s ease-out;
      .btn{
        transform: translateY(0);
        opacity: 1;
      }
    }
    &.animated{
      .container{
        transform: translateY(0);
        opacity: 1;
      }
    }
  }