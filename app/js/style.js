(function ($) {

  $(document).ready(function () {

    /*menu scroll*/
    function anchorScrollAnimation(target) {
      $('section').each(function () {
        var thisId = $(this).attr('id'),
          thisOffset = $(this).offset().top;
        if (target == thisId) {
          $('html, body').stop().animate({
            scrollTop: thisOffset - 80,
            easing: "easein"
          }, 1500);
        }
      });
    }

    if (window.location.hash != '') {
      //console.log('hash', location.hash)
      window.scrollTo(0, 0);
      var lh = window.location.hash.slice(1);
      anchorScrollAnimation(lh);
      location.hash = '';
    }
    else {
      //console.log('else')
      $('.menu-item a').on('click', function (event) {
        if ($(this).attr('aria-current')) {
          event.preventDefault();
          //console.log($(this).attr('href').split("#").slice(-1).toString())
          var id = $(this).attr('href').split("#").slice(-1).toString(),
            //узнаем высоту от начала страницы до блока на который ссылается якорь
            top = $("#" + id).position().top;
          //анимируем переход на расстояние - top за 1000 мс
          if ($(window).width() < 768) {
            $('.menu-mobile').removeClass('active');
            $('header').removeClass('active');
            $('body').removeClass('overflow-menu');
          }
          $('html, body')
            .stop()
            .animate(
              {
                scrollTop: top - 80,
                easing: 'easein',
              },
              1000
            );
        }
      });
    }

    $('.menu-item a[aria-current]').on('click', function (event) {
      console.log(location.hash)
      if (location.hash == '') {
        event.preventDefault();
        //console.log($(this).attr('href').split("#").slice(-1).toString())
        var id = $(this).attr('href').split("#").slice(-1).toString(),
          //узнаем высоту от начала страницы до блока на который ссылается якорь
          top = $("#" + id).position().top;
        //анимируем переход на расстояние - top за 1000 мс
        if ($(window).width() < 768) {
          $('.menu-mobile').removeClass('active');
          $('header').removeClass('active');
          $('body').removeClass('overflow-menu');
        }
        $('html, body')
          .stop()
          .animate(
            {
              scrollTop: top - 80,
              easing: 'easein',
            },
            1000
          );
      }
    })

    $("a[href^='#']").on('click', function (e) {
      e.preventDefault();
      var target = this.hash;
      var target_offset = $(target).offset() ? $(target).offset().top : 0;
      $('html, body').animate({
        scrollTop: target_offset - 80
      }, 1000, function () {
      });
    });

    /*mobile menu */
    $('.menu-mobile').on('click', function () {
      if ($(this).hasClass('active')) {
        $('header').removeClass('active');
        $(this).removeClass('active');
        $('body').removeClass('overflow-menu');
      }
      else {
        $('header').addClass('active');
        $(this).addClass('active');
        $('body').addClass('overflow-menu');
      }
    });


    /* scrollbar on page */
    //$('.body-wrapper').scrollbar();
    if ($(window).width() < 768) {
      $('.mobile-open').scrollbar();
    }

    /*var $quote = $(".underline p"),
      mySplitText = new SplitText($quote, { type: "words" });
    mySplitText.split({ type: "lines" });*/

    //new SplitText(".underline p", { type: 'lines', })


    /* slider 
    var swiper = new Swiper('.swiper-container', {
      loop: true,
      autoplay: {
        delay: 5000,
      },
      slidesPerView: 6,
      spaceBetween: 20,
      slidesPerGroup: 6,
      //loop: true,
      //loopFillGroupWithBlank: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      breakpoints: {
        // when window width is >= 320px
        320: {
          slidesPerView: 3,
          spaceBetween: 10
        },
        599: {
          slidesPerView: 2,
          spaceBetween: 15
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 15
        },
        // when window width is >= 640px
        1999: {
          slidesPerView: 6,
          spaceBetween: 20
        }
      }
    });
    $(".swiper-container").mouseenter(function () {
      swiper.autoplay.stop();
    });
    $(".swiper-container").mouseleave(function () {
      swiper.autoplay.start();
    });

*/

    /*header */
    var controller = new ScrollMagic.Controller({ globalSceneOptions: { duration: 100 } });

    var headerSticky = new TimelineMax();
    headerSticky
      .to("header", 0.3, { className: "+=sticky" })

    var sceneHeader = new ScrollMagic.Scene({
      triggerElement: '.intro',
      triggerHook: 0,
    })
      .setTween(headerSticky)
      .addTo(controller);


    /*animation */
    setTimeout(function () {
      $('.preloader')
        .stop()
        .addClass('hidden');
      setTimeout(function () {
        $('.animation').each(function () {
          const thisController = new ScrollMagic.Controller();

          var contentAnimation = new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: 0.7,
            reverse: false,
          })
            .setClassToggle(this, 'animated')
            .addTo(thisController);
        });
      }, 700);
    }, 2000);


    $('.blocks > div').on('mouseover', function () {
      $(this).find('.hover-blocks').removeClass('leave').addClass('hover');
    })
    $('.blocks > div').on('mouseout', function () {
      $(this).find('.hover-blocks').removeClass('hover').addClass('leave');
    })

    /*parallax 
    document.querySelectorAll('*[data-from]').forEach(box => {
      const thisController = new ScrollMagic.Controller();

      let thisFromPercent = box.getAttribute('data-from'),
        thisToPercent = box.getAttribute('data-to'),
        thisImg = box.children[0];

      const parallaxItem = new TimelineMax().fromTo(
        thisImg,
        1,
        { y: thisFromPercent },
        { y: thisToPercent, ease: Linear.easeNone }
      );

      new ScrollMagic.Scene({
        triggerElement: box,
        reverse: true,
        triggerHook: 0.9,
        duration: '160%',
      })
        .setTween(parallaxItem)
        .addTo(thisController);
    });
*/













  });

  /*window load*/
  $(window).on('load', function () {
    if ($(window).width() > 768) {
      var duration = $('.blocks-overlay:nth-child(2)').outerHeight() - $(window).height();
      console.log(duration);
      var controller = new ScrollMagic.Controller();

      new ScrollMagic.Scene({
        triggerElement: '.test-trigger',
        triggerHook: 0,
        duration: duration
      })
        .setPin('.features-page')
        //.addIndicators()
        .addTo(controller);

      $(window).on('scroll', function () {
        $('.blocks').scrollTop($(this).scrollTop());
      });

      document.querySelectorAll('#features.features-page .blocks .blocks-overlay > div').forEach(box => {
        var controller1 = new ScrollMagic.Controller({ container: "#features.features-page .blocks" });
        // build scene

        var scene = new ScrollMagic.Scene({ triggerElement: box, triggerHook: 0.8, duration: 300 })
          .addTo(controller1)
          .setTween(TweenMax.to(box, 0.5, {
            opacity: 1, ease:
              Power0.easeNone
          }))
        //.addIndicators() // add indicators (requires plugin)
        //.setPin(box, { pushFollowers: true });
      })
    }

  });


  /*window resize*/
  $(window).resize(function () {
    //new SplitText(".underline p", { type: 'lines', })

  });

})(jQuery);